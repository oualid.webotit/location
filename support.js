module.exports = {
    Label: 'Support',
    Dialog: function (session) {
        // Generate ticket
        var tickerNumber = Math.ceil(Math.random() * 20000);

        // Reply and return to parent dialog
        session.send('Votre message \'%s\' a été enregistré avec le numéro : ', session.message.text);
        session.endDialogWithResult({
            response: tickerNumber
        });
    }
};