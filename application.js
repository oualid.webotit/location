
var builder = require('botbuilder');
var restify = require('restify');

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
    console.log('%s listening to %s', server.name, server.url);
});
/////
// Create chat bot
var connector = new builder.ChatConnector({
    appId: '3bedb808-ad3f-4c36-8cec-c8437af05ba5',
    appPassword: 'jeks8okGibymit4r8bvpYaq'
});
var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());
// Dialogs
var Hotels = require('./hotels');
var Flights = require('./flights');
var Support = require('./support');

// Setup dialogs
bot.dialog('/voiture', Flights.Dialog);
bot.dialog('/moto', Hotels.Dialog);
bot.dialog('/support', Support.Dialog);

// Root dialog
bot.dialog('/', new builder.IntentDialog()
    .matchesAny([/help/i, /support/i, /problem/i], [
        function (session) {
            session.beginDialog('/support');
        },
        function (session, result) {
            var tickerNumber = result.response;
            session.send('Merci de contacter le support par mail : walid.barjani@gmail.com');
            session.endDialog();
        }
    ])
    .onDefault([
        function (session) {
            // prompt for search option
            builder.Prompts.choice(
                session,
                'Cherchez vous à louer une voiture ou une moto ?',
                [Flights.Label, Hotels.Label],
                {
                    maxRetries: 3,
                    retryPrompt: 'Non valide !'
                });
        },
        function (session, result) {
            if (!result.response) {
                // exhausted attemps and no selection, start over
                session.send('Vous avez trop essayé ! Mais vous pouvez recommencer ;) ');
                return session.endDialog();
            }

            // on error, start over
            session.on('error', function (err) {
                session.send('Erreur du message: %s', err.message);
                session.endDialog();
            });

            // continue on proper dialog
            var selection = result.response.entity;
            switch (selection) {
                case Flights.Label:
                    return session.beginDialog('/voiture')
                case Hotels.Label:
                    return session.beginDialog('/moto')
            }
        }
    ]));

