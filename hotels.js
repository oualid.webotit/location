var builder = require('botbuilder');
var Store = require('./store');

module.exports = {
    Label: 'Voiture',//fgdsfgdsgd
    Dialog: [
        // Destination
        function (session) {
            session.send('Bienvenue sur la location de voiture !');
            builder.Prompts.text(session, 'Veuillez saisir la marque de la voiture');
        },
        function (session, results, next) {
            session.dialogData.destination = results.response;
            session.send('Recherche d\'une voiture de marque %s', results.response);
            next();
        },

        // Check-in//////
        function (session) {
            builder.Prompts.time(session, 'Veuillez saisir la date début de location');
        },
        function (session, results, next) {
            session.dialogData.checkIn = results.response.resolution.start;
            next();
        },

        // Nights
        function (session) {
            builder.Prompts.number(session, 'Pour combien de jours ?');
        },
        function (session, results, next) {
            session.dialogData.nights = results.response;
            next();
        },

        // Search...
        function (session) {
            var destination = session.dialogData.destination;
            var checkIn = new Date(session.dialogData.checkIn);
            var checkOut = checkIn.addDays(session.dialogData.nights);

            session.send(
                'Ok. Recherche de voiture de marque %s entre le %d/%d et le %d/%d...',
                destination,
                checkIn.getMonth() + 1, checkIn.getDate(),
                checkOut.getMonth() + 1, checkOut.getDate());

            // Async search
            Store
                .searchHotels(destination, checkIn, checkOut)
                .then((hotels) => {
                    // Results
                    session.send('Bingo ! J\'ai trouvé en total %d voitures pour votre séjour :', hotels.length);

                    var message = new builder.Message()
                        .attachmentLayout(builder.AttachmentLayout.carousel)
                        .attachments(hotels.map(hotelAsAttachment));

                    session.send(message);

                    // End
                    session.endDialog();
                });
        }
    ]
};

// Helpers
function hotelAsAttachment(hotel) {
    return new builder.HeroCard()
        .title(hotel.name)
        .subtitle('Kilométrage %d. %d avis. A partir de %d€ par jour.', hotel.rating, hotel.numberOfReviews, hotel.priceStarting)
        .images([new builder.CardImage().url(hotel.image)])
        .buttons([
            new builder.CardAction()
                .title('Plus de détails')
                .type('openUrl')
                .value('https://www.google.com/search?q=voiture+location+' + encodeURIComponent(hotel.location))
        ]);
}

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}