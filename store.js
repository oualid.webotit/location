var Promise = require('bluebird');

module.exports = {
    searchHotels: function (destination, checkInDate, checkOutDate) {
        return new Promise(function (resolve) {

            // Filling the hotels results manually just for demo purposes
            var hotels = [];
            for (var i = 1; i <= 5; i++) {
                hotels.push({
                    name: 'Voiture de marque ' + destination,
                    location: destination,
                    rating: Math.ceil(Math.random() * 50000),
                    numberOfReviews: Math.floor(Math.random() * 10) + 1,
                    priceStarting: Math.floor(Math.random() * 250) + 80,
                    image: 'https://placeholdit.imgix.net/~text?txtsize=45&txt=Voiture+' + i + '&w=500&h=260'
                });
            }

            hotels.sort((a, b) => a.priceStarting - b.priceStarting);

            // complete promise with a timer to simulate async response
            setTimeout(() => resolve(hotels), 1000);
        });
    }
};